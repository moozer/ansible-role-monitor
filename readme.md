To use

1. Start the vma nd provision it: `vagrant up`
2. Find the ip address of the box: `vagrant ssh-config`
3. Use a browser and go to `http://<ip-address>:3000
3. Login: admin:adminpassforgrafana

Add loki datasource

4. Add a source:
    * Type: loki
    * Name: Localhost loki
    * URL: http://127.0.0.1:3100
5. Click "save and test"
6. Import dashboard: click '+' from the left and select import
7. Upload json file: `files/dashboard_logs_overview.json` and click `load`
8. Go to the dashboard and verify that it works.

Add prometheus datasource

9. Add a source
    * Type: Prometheus
    * Name: Prometheus
    * URL: http://localhost:9090
5. Click "save and test"
6. Import dashboard: click '+' from the left and select import
7. Upload json file: `files/dashboard_prometheus_test.json` and click `load`
8. Go to the dashboard and verify that it works.


Loki cheatsheet
* https://levelup.gitconnected.com/loki-installation-in-ubuntu-2eb8407de291?gi=33317b6631d5

refs:
* https://computingforgeeks.com/forward-logs-to-grafana-loki-using-promtail/
* https://computingforgeeks.com/how-to-install-prometheus-and-node-exporter-on-debian/